/*
 * gqextio.c
 *
 *  Created on: Jul 5, 2014
 *      Author: bitcraze
 */

#include "gqextio.h"

#include <stdbool.h>
#include "stm32f10x_conf.h"

/*FreeRtos includes*/
#include "FreeRTOS.h"
#include "task.h"

static bool isInit=false;

static GPIO_TypeDef* extio_port[] = {
  [CAPTUURE] = EXTIO_GPIO_PORT,
  [CAPTUURE_EN1] = EXTIO_GPIO_PORT,
  [CAPTUURE_EN2] = EXTIO_GPIO_PORT,
};
static unsigned int extio_pin[] = {
  [CAPTUURE] = EXTIO_GPIO_CAP,
  [CAPTUURE_EN1] = EXTIO_GPIO_EN1,
  [CAPTUURE_EN2] = EXTIO_GPIO_EN2,
};
static int extio_polarity[] = {
  [CAPTUURE] = EXTIO_GPIO_CAP,
  [CAPTUURE_EN1] = EXTIO_GPIO_EN1,
  [CAPTUURE_EN2] = EXTIO_GPIO_EN2,
};


void extioInit(){
	if(isInit)
		return;

	GPIO_InitTypeDef GPIO_InitStructure;

	// Enable GPIO
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO | EXTIO_GPIO_PERIF, ENABLE);

	// Remap PB4
	//GPIO_PinRemapConfig(GPIO_Remap_SWJ_NoJTRST , ENABLE);
	//GPIO_Remap_SWJ_Disable
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_Disable , ENABLE);

	//Initialize the LED pins as an output
	GPIO_InitStructure.GPIO_Pin = EXTIO_GPIO_CAP | EXTIO_GPIO_EN1 | EXTIO_GPIO_EN2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;

	GPIO_Init(EXTIO_GPIO_PORT, &GPIO_InitStructure);

	//Turn off the LED:s
	capSet(CAPTUURE, 1);
	capSet(CAPTUURE_EN1, 0);
	capSet(CAPTUURE_EN2, 0);
	//ledSet(LED_RED, 0);

	isInit = true;
}

void capSet(extio_t led, bool value) {

  if (extio_polarity[led]==EXTIO_POL_NEG)
    value = !value;

  if(value){
	  GPIO_SetBits(extio_port[led], extio_pin[led]);
  }else{
	  GPIO_ResetBits(extio_port[led], extio_pin[led]);
  }

#ifdef MOTORS_TEST
  if(led == LED_RED) {
    static int step = 0;

    if(!value)
    {
      motorsSetRatio(step, 0x3FFF);

      step++;
      if(step>3) step=0;
    }
    else
    {
      motorsSetRatio(0, 0x0000);
      motorsSetRatio(1, 0x0000);
      motorsSetRatio(2, 0x0000);
      motorsSetRatio(3, 0x0000);
    }
  }
#endif
}
