/*
 * gqextio.h
 *
 *  Created on: Jul 5, 2014
 *      Author: bitcraze
 */

#include <stdbool.h>

#include "stm32f10x_conf.h"

#ifndef GQEXTIO_H_
#define GQEXTIO_H_

//Led polarity configuration constant
#define EXTIO_POL_POS 0
#define EXTIO_POL_NEG 1

//Hardware configuration
#define EXTIO_GPIO_PERIF   RCC_APB2Periph_GPIOA
#define EXTIO_GPIO_PORT    GPIOA

#define EXTIO_GPIO_CAP   GPIO_Pin_13
#define EXTIO_POL_CAP    EXTIO_POL_NEG

#define EXTIO_GPIO_EN1   GPIO_Pin_14
#define EXTIO_POL_EN1    EXTIO_POL_NEG

#define EXTIO_GPIO_EN2     GPIO_Pin_15
#define EXTIO_POL_EN2      EXTIO_POL_NEG

typedef enum {CAPTUURE=0 , CAPTUURE_EN1=1, CAPTUURE_EN2=2} extio_t;

void extioInit();
void capSet(extio_t led, bool value);

#endif /* GQEXTIO_H_ */
